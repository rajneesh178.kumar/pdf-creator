//
//  FileMgr.swift
//  PDFApp
//
//  Created by Rajneesh Kumar on 24/05/20.
//  Copyright © 2020 Rajneesh Kumar. All rights reserved.
//

import UIKit


class FileMgr: NSObject {
    
    static let fileManager = FileManager.default
    
    class func createFilePath(fileName : String) -> URL?{
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("\(fileName).pdf") {
            let filePath = pathComponent.path
            
            if fileManager.fileExists(atPath: filePath) {
                return pathComponent.absoluteURL
            } else {
                fileManager.createFile(atPath: filePath, contents: nil, attributes: nil)
                return pathComponent.absoluteURL
            }
        } else {
            print("FILE PATH NOT AVAILABLE")
        }
        return nil
    }
    
    //Save the pdf file into our documnet directory
    class func saveFileIntoDirectory(pdfFileData : Data? , fileName : String , completionHandler : (Bool) -> ()){
    
         
         do {
             let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
             let fileURL = documentDirectory.appendingPathComponent("\(fileName).pdf")
             if let pdfData = pdfFileData {
                 try pdfData.write(to: fileURL)
                 debugPrint(fileURL)
                completionHandler(true)
             }
         } catch {
             print(error)
             completionHandler(false)
         }
     }

}
