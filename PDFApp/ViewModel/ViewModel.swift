//
//  ViewModel.swift
//  PDFApp
//
//  Created by Rajneesh Kumar on 24/05/20.
//  Copyright © 2020 Rajneesh Kumar. All rights reserved.
//

import UIKit

protocol FileProtocol {
    func getFilesSucessfully()
}

class ViewModel: NSObject {
    
   var delegate : FileProtocol?
   private var pdfFiles : [FileModel]?
    
    func getFiles(){
        let pdfFileUrl = Bundle.main.url(forResource: "ios_tutorial", withExtension: "pdf")
        
        guard let fileUrl = pdfFileUrl else { return }
        let fileData = FileModel(fileName: "Original File", isEditable: true, pdfUrl: fileUrl)
        pdfFiles?.removeAll()
        pdfFiles = [fileData]
        getFilesFromDocumentDirectory()
        
        //push the delgate here to render in UI.
        delegate?.getFilesSucessfully()
    }
    
    
    private func getFilesFromDocumentDirectory(){
        // Get the document directory url
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
            print(directoryContents)

            // if you want to filter the directory contents you can do like this:
            let pdfFileUrl = directoryContents.filter{ $0.pathExtension == "pdf" }
            let fileNames = pdfFileUrl.map{ $0.deletingPathExtension().lastPathComponent }
            
            if fileNames.count == pdfFileUrl.count {
                for i in 0 ..< pdfFileUrl.count {
                    let fileName = fileNames[i]
                    let fileUrl = pdfFileUrl[i]
                    let fileObj = FileModel(fileName: fileName, isEditable: false, pdfUrl: fileUrl)
                    pdfFiles?.append(fileObj)
                }
            }
        } catch {
            print(error)
        }
    }
    
    func getRows() -> Int {
        guard let files = pdfFiles else { return 0}
        return files.count
    }
    
    func getFile(for index : Int) -> FileModel? {
        guard let files = pdfFiles else { return nil}
        guard files.indices.contains(index) else { return nil }
        return files[index]
    }
}
