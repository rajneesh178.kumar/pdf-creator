//
//  PdfEditViewController.swift
//  PDFApp
//
//  Created by Rajneesh Kumar on 24/05/20.
//  Copyright © 2020 Rajneesh Kumar. All rights reserved.
//

import UIKit
import  PDFKit

class PdfEditViewController: UIViewController {
    
    @IBOutlet weak var fileNameTxtFiled: UITextField!
    @IBOutlet weak var startIndexTxtField: UITextField!
    @IBOutlet weak var endIndexTxtFiled: UITextField!
    
    var fileObj : FileModel?
    var newPdfPages = [PDFPage]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
}



//MARK: - Custem Method Here

extension PdfEditViewController {
    
    //Create the Pdf file as per user data
    func createPdfFile(){
        guard let originalPdfFile = PDFDocument(url: fileObj!.pdfUrl) else { return }
        guard let statText = startIndexTxtField.text , let endText = endIndexTxtFiled.text else { return }
        guard let startIndex = Int(statText) , let endIndex = Int(endText) else { return }
        
        for i in startIndex ... endIndex{
            if let pdfPage = originalPdfFile.page(at: i){
                newPdfPages.append(pdfPage)
            }else {
                debugPrint("Not Found page")
            }
        }
        //Now convert all pages into one pdf file
        getNewCreatedFile()
    }
    
    
    //Convert all pages into one pdf file
    func getNewCreatedFile(){
        let pdfFileUrl = Bundle.main.url(forResource: "dummy", withExtension: "pdf")
        
        guard let fileUrl = pdfFileUrl else { return }
        guard let pdfFile = PDFDocument(url: fileUrl) else { return }
        
        //First we need to clear all pages form the dummy pdf , otherwise it repeat the content again.
        for i in 0 ... pdfFile.pageCount{
            pdfFile.removePage(at: 0)
        }
        
       //Now Write the new page data
        for (index, pdfPage) in newPdfPages.enumerated() {
            pdfFile.insert(pdfPage, at: index)
            pdfFile.write(to: fileUrl)
        }
        saveFileIntoDirectory(pdfFile: pdfFile)
    }
    
    
    
    //Saev the pdf file into our documnet directory
    func saveFileIntoDirectory(pdfFile : PDFDocument){
        //Saved the file here -
        let fileName = fileNameTxtFiled.text ?? "New File"
        let pdfFileData = pdfFile.dataRepresentation()
        FileMgr.saveFileIntoDirectory(pdfFileData: pdfFileData, fileName: fileName) { (result) in
            guard result else {
                showErrorAlert()
                return
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //Show the error Alert
    func showErrorAlert(){
          let alert = UIAlertController(title: "", message: "Something went worng for file saving .", preferredStyle: .alert)
          let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
          alert.addAction(cancelAction)
          self.present(alert, animated: true, completion: nil)
      }
}




//MARK: - @IBAction Method Here

extension PdfEditViewController {
    
    @IBAction func createPdfButtonPressed(_ sender: Any) {
        createPdfFile()
    }
}





//MARK: - UITextFieldDelegate Here

extension PdfEditViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
    }
}
