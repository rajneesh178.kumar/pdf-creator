//
//  ViewController.swift
//  PDFApp
//
//  Created by Rajneesh Kumar on 24/05/20.
//  Copyright © 2020 Rajneesh Kumar. All rights reserved.
//

import UIKit
import  PDFKit

class ViewController: UIViewController {
    
    //IBOutlet Declaration
    @IBOutlet weak var fileTableView: UITableView!
    
    //Variable Declaration
    private let viewVM = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        viewVM.delegate = self
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewVM.getFiles()
    }
}




//MARK: - @IBAction method

extension ViewController {
    
    @IBAction func mergeButtonPresesd(_ sender: Any) {
        //becaise currently i am ignoring original file
        if viewVM.getRows() >= 3 {
            guard let firstPdfObj = viewVM.getFile(for: 1) , let secondPdfObj = viewVM.getFile(for: 2) else { return }
            //now go for merge but first we need to create the path where we can save our file-
            if let destinationURl = FileMgr.createFilePath(fileName: "Merged Pdf File"){
                mergeTwoPdfFiles(firstPdfURL: firstPdfObj.pdfUrl, secondPdfURL: secondPdfObj.pdfUrl, destinationPath: destinationURl)
            }
        }else {
            showErrorAlert()
        }
    }
}



//MARK: - Custem method

extension ViewController {
    
    private func configureTableView(){
        fileTableView.delegate = self
        fileTableView.dataSource = self
        
        let fileCell = UINib(nibName: "FileCell", bundle: nil)
        self.fileTableView.register(fileCell, forCellReuseIdentifier: "FileCell")
        
        self.fileTableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    
    @objc private func editButtonPressed(_ sender: UIButton) {
        
        guard let fileObj = viewVM.getFile(for: sender.tag) else { return }
        let passingContoller = self.storyboard?.instantiateViewController(identifier: "PdfEditViewController") as! PdfEditViewController
        passingContoller.fileObj = fileObj
        self.navigationController?.pushViewController(passingContoller, animated: true)
    }
    
    
    private func openPdfFile(fileUrl : URL?){
        let pdfController = UIViewController()
        pdfController.view.frame = self.view.frame
        
        let pdfView = PDFView(frame: self.view.frame)
        pdfController.view.addSubview(pdfView)
        
        guard let pdfUrl = fileUrl , let document = PDFDocument(url: pdfUrl) else  { return }
        
        pdfView.autoresizesSubviews = true
        pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin, .flexibleLeftMargin]
        pdfView.displayDirection = .vertical
        
        pdfView.autoScales = true
        pdfView.displayMode = .singlePageContinuous
        pdfView.displaysPageBreaks = true
        pdfView.document = document
        
        pdfView.maxScaleFactor = 4.0
        pdfView.minScaleFactor = pdfView.scaleFactorForSizeToFit
        self.present(pdfController, animated: true, completion: nil)
    }
    
    
    func mergeTwoPdfFiles(firstPdfURL : URL , secondPdfURL : URL , destinationPath : URL){
        //fIRST CONVERT INTO PDF DOCUMENT
        let destinatnUrl = destinationPath
        guard let firstPdfFile = PDFDocument(url: firstPdfURL) , let secondPdfFile = PDFDocument(url: secondPdfURL) else {
            debugPrint("url not ofind")
            return }
        
        var pageCount = firstPdfFile.pageCount
        
        //NOW FETCH THE PAGE THAT WE NEED TO ADD IN FIRST PDF -
        for i in 0 ..< secondPdfFile.pageCount {
            if let newPage = secondPdfFile.page(at: i){
                firstPdfFile.insert(newPage, at: pageCount)
                firstPdfFile.write(to: destinationPath)
            }
            pageCount += 1
        }
        //NOW THIS TIME TO SAVE NEW PDF FILE
        if let pdfData = firstPdfFile.dataRepresentation() {
            FileMgr.saveFileIntoDirectory(pdfFileData: pdfData, fileName: "Merged Pdf File") { (result) in
                debugPrint("Merger sucessfully")
                viewVM.getFiles()
            }
        }
    }
    
    func showErrorAlert(){
        let alert = UIAlertController(title: "", message: "Please add upto two new pdf files for merge", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}





//MARK: - UITableViewDelegate , UITableViewDataSource

extension ViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewVM.getRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let fileCell = tableView.dequeueReusableCell(withIdentifier: "FileCell") as! FileCell
        
        //here we get the fileObj for each index
        let fileObj = viewVM.getFile(for: indexPath.row)
        
        //Here we configure cell ui
        fileCell.configureFileCell(fileObj: fileObj)
        
        //Here we configure edit button
        if fileObj?.isEditable ?? true{
            fileCell.editButton.isHidden = false
            fileCell.editButton.tag = indexPath.row
            fileCell.editButton.addTarget(self, action: #selector(editButtonPressed(_:)), for: .touchUpInside)
        }else {
            fileCell.editButton.isHidden = true
        }
        
        return fileCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fileObj = viewVM.getFile(for: indexPath.row)
        openPdfFile(fileUrl: fileObj?.pdfUrl)
    }
}




//MARK: - FileProtocol
extension ViewController : FileProtocol {
    func getFilesSucessfully() {
        self.fileTableView.reloadData()
    }
}

