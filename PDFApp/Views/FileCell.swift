//
//  FileCell.swift
//  PDFApp
//
//  Created by Rajneesh Kumar on 24/05/20.
//  Copyright © 2020 Rajneesh Kumar. All rights reserved.
//

import UIKit

class FileCell: UITableViewCell {

    @IBOutlet weak var fileNameLbl: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    func configureFileCell(fileObj : FileModel?){
        guard let file = fileObj else { return }
        fileNameLbl.text = file.fileName
    }
}
