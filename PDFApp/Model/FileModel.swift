//
//  FileModel.swift
//  PDFApp
//
//  Created by Rajneesh Kumar on 24/05/20.
//  Copyright © 2020 Rajneesh Kumar. All rights reserved.
//
import Foundation

struct FileModel {
    let fileName : String
    let isEditable : Bool
    let pdfUrl : URL
}
